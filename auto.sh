#!/bin/bash

# pacman -Sy git
mkdir -p /mnt/home/$user/Desktop
cd /mnt/home/$user/Desktop

git clone https://gitlab.com/shastenm76/arch-xfce

chmod 777 arch-xfce

cd arch-xfce

chmod +x *.sh

chmod 777 pacman.conf
chmod 777 sudoers
#chmod 777 grub
# chmod 777 lightdm.conf
# chmod 777 display_setup

rm -rf /mnt/home/shasten/Desktop/xfce_auto_install


./01-archinstall-base.sh

